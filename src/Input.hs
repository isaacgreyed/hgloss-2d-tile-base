module Input
    (handleKeys)
    where

      import Graphics.Gloss

      import Graphics.Gloss.Data.ViewPort
      import Graphics.Gloss.Interface.Pure.Game

      import qualified Data.Set as S
      import Render
      import World
      
      handleKeys :: Event -> Game -> Game
      handleKeys (EventKey k Down _ _) (Game system grid) = Game System {keys = S.insert k (keys system), viewport = viewport system} grid
      handleKeys (EventKey k Up   _ _) (Game system grid) = Game System {keys = S.delete k (keys system), viewport = viewport system} grid
      handleKeys _                     game = game
