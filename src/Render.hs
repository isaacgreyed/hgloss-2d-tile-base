module Render
      (background, render, window)
      where

        import Graphics.Gloss

        import Graphics.Gloss.Data.ViewPort
        import Graphics.Gloss.Interface.Pure.Game

        import World

        window :: Display
        window = InWindow "Basic Gloss Engine" (1920, 1080) (1440,0)

        background :: Color
        background = white

        render :: Game -> Picture
        render (Game system grid) = pictures
          [ rectangleWire 1005 1005
          , drawGrid grid size start start
          ]
            where size = 1000 / fromIntegral (length grid)
                  halfSize = size / 2
                  start = length grid `div` 2 - length grid

        drawTile :: Tile -> Float -> Picture
        drawTile tile size = color (object2color $ last tile) $ rectangleSolid size size

        object2color :: Object -> Color
        object2color (Floor color) = color
        object2color (Entity color) = color
        --object2color object = white

        drawRow :: Row -> Float -> Int -> Picture
        drawRow [] _ _ = pictures []
        drawRow row size count = pictures [translate adjustment 0 $ drawTile (head row) size, drawRow (tail row) size (count+1)]
                                    where halfSize = size / 2
                                          adjustment = fromIntegral count * size + halfSize

        drawGrid :: Grid -> Float -> Int -> Int -> Picture
        drawGrid [] _ _ _ = pictures []
        drawGrid grid size count c = pictures [translate 0 adjustment $ drawRow (head grid) size c, drawGrid (tail grid) size (count+1) c]
                                    where halfSize = size / 2
                                          adjustment = fromIntegral count * size + halfSize
