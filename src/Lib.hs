module Lib
    ( someFunc
    ) where

import Graphics.Gloss

import Graphics.Gloss.Data.ViewPort
import Graphics.Gloss.Interface.Pure.Game

import qualified Data.Set as S

import Render
import World
import Input


update :: Float -> Game -> Game
update time game = game

engine :: IO () --the main gloss function
engine = play window background 60 initGame render handleKeys update

someFunc = engine
