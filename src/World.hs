module World
        where
          import Graphics.Gloss

          import Graphics.Gloss.Data.ViewPort
          import Graphics.Gloss.Interface.Pure.Game

          import qualified Data.Set as S


          data Object = Floor Color | Entity Color --any object the game needs a type for

          type Tile = [Object]

          type Row = [Tile]
          type Column = Row

          type Grid = [Row]

          initTile :: Tile
          initTile = [Floor $ greyN 0.5]

          initRow :: Int -> Row
          initRow width = replicate width initTile

          initGrid :: Int -> Grid
          initGrid size = replicate size (initRow size)


          data System = System {
              keys :: S.Set Key
            , viewport :: ViewPort
          }

          initSystem :: System
          initSystem = System {keys = S.empty, viewport = viewPortInit}


          data Game = Game System Grid

          initGame :: Game
          initGame = Game initSystem (initGrid 10)
